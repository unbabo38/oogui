require 'test_helper'

class NameControllerTest < ActionDispatch::IntegrationTest
  test "should get search" do
    get name_search_url
    assert_response :success
  end

end
