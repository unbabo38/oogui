class AddAlbumsToArtists < ActiveRecord::Migration[5.2]
  def change
    add_column :artists, :albums, :string
    add_column :artists, :songs, :string
  end
end
