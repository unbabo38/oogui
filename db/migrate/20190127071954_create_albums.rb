class CreateAlbums < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.text :mbids
      t.text :albums
      t.text :songs

      t.timestamps
    end
  end
end
