class AddMbidToArtists < ActiveRecord::Migration[5.2]
  def change
    add_column :artists, :mbid, :Integer
  end
end
