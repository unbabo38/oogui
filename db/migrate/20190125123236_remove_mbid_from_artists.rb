class RemoveMbidFromArtists < ActiveRecord::Migration[5.2]
  def change
    remove_column :artists, :mbid, :string
  end
end
