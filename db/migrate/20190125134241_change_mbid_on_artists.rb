class ChangeMbidOnArtists < ActiveRecord::Migration[5.2]
  def change
   change_column :artists, :mbid, :text
  end
end
