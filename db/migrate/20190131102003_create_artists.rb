class CreateArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :artists do |t|
      t.text :artists
      t.text :mbids

      t.timestamps
    end
  end
end
