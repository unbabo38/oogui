class CreateNames < ActiveRecord::Migration[5.2]
  def change
    create_table :names do |t|
      t.string :subject
      t.integer :price

      t.timestamps
    end
  end
end
