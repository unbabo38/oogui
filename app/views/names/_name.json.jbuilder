json.extract! name, :id, :subject, :price, :created_at, :updated_at
json.url name_url(name, format: :json)
