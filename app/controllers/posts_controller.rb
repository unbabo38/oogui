class PostsController < ApplicationController
 def create
   @post = Post.new(content:params[:content])
   @post.save
 end 

 def show
   @post = Post.find_by(id:params[:id])
 end
end
