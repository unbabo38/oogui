class Post2Controller < ApplicationController

  def index
   @posts = Post2.all.order(created_at: 'desc')

   @response = Net::HTTP.get(URI.parse('https://musicbrainz.org/ws/2/release?label=47e718e1-7ee4-460c-b1cc-1192a841c6e5'))
  end
  
  def show
    @post = Post2.find(params[:id])
  end

  def new
	  @post = Post2.new
  end

  def create
    #render plain: params[:post].inspect
	  @post = Post2.new(post_params)
	  if @post.save
    redirect_to post2_index_path
	  else
	    render 'new'
             
  end
end
 private
 def post_params
    params.require(:post2).permit(:title, :body)
 end


end
