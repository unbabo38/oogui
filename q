=> [32m#<MusicBrainz::Artist:0x0000563bd92a0dd0[0m
 @country[32m=[0m[1;36mnil[0m,
 @date_begin[32m=[0m[1;36mnil[0m,
 @date_end[32m=[0m[1;36mnil[0m,
 @id[32m=[0m[31m[1;31m"[0m[31m88130878-7ee9-4ce2-b24b-1dd9da898030[1;31m"[0m[31m[0m,
 @name[32m=[0m[31m[1;31m"[0m[31mMiles Davis Septet[1;31m"[0m[31m[0m,
 @release_groups[32m=[0m
  [[32m#<MusicBrainz::ReleaseGroup:0x0000563bd6edb698[0m
    @disambiguation[32m=[0m[1;36mnil[0m,
    @first_release_date[32m=[0m[1;34m2009[0m-[1;34m12[0m-[1;34m31[0m [1;34m00[0m:[1;34m00[0m:[1;34m00[0m [1;34;4mUTC[0m,
    @id[32m=[0m[31m[1;31m"[0m[31m3f18c00c-60f3-44eb-abb0-8c3b5d60fdeb[1;31m"[0m[31m[0m,
    @releases[32m=[0m
     [[32m#<MusicBrainz::Release:0x0000563bd9325288[0m
       @country[32m=[0m[31m[1;31m"[0m[31mXE[1;31m"[0m[31m[0m,
       @date[32m=[0m[1;34m20092009[0m-[1;34m01[0m-[1;34m01[0m [1;34m00[0m:[1;34m00[0m:[1;34m00[0m [1;34;4mUTC[0m,
       @format[32m=[0m[31m[1;31m"[0m[31mCD[1;31m"[0m[31m[0m,
       @id[32m=[0m[31m[1;31m"[0m[31m8ae27f2d-54f7-4d2b-b518-d0675c6f9df4[1;31m"[0m[31m[0m,
       @status[32m=[0m[31m[1;31m"[0m[31mBootleg[1;31m"[0m[31m[0m,
       @title[32m=[0m[31m[1;31m"[0m[31mLive in Vienna 1973[1;31m"[0m[31m[0m,
       @tracks[32m=[0m
        [[32m#<MusicBrainz::Track:0x0000563bd6dca808[0m
          @length[32m=[0m[1;34m704000[0m,
          @position[32m=[0m[1;34m1[0m,
          @recording_id[32m=[0m[31m[1;31m"[0m[31mb340399d-8d91-4f20-a3c7-d941b723f9ac[1;31m"[0m[31m[0m,
          @title[32m=[0m[31m[1;31m"[0m[31mTurnaroundphrase[1;31m"[0m[31m[0m[32m>[0m,
         [32m#<MusicBrainz::Track:0x0000563bd6dc76d0[0m
          @length[32m=[0m[1;34m341000[0m,
          @position[32m=[0m[1;34m2[0m,
          @recording_id[32m=[0m[31m[1;31m"[0m[31m0186f3b7-e22c-4098-9d20-6c4ba785e7ca[1;31m"[0m[31m[0m,
          @title[32m=[0m[31m[1;31m"[0m[31mTune in 5[1;31m"[0m[31m[0m[32m>[0m,
         [32m#<MusicBrainz::Track:0x0000563bd93c1fc0[0m
          @length[32m=[0m[1;34m766000[0m,
          @position[32m=[0m[1;34m3[0m,
          @recording_id[32m=[0m[31m[1;31m"[0m[31m55acf7fc-f1dd-4ede-8be3-36dfe8e2c49f[1;31m"[0m[31m[0m,
          @title[32m=[0m[31m[1;31m"[0m[31mIfe[1;31m"[0m[31m[0m[32m>[0m,
         [32m#<MusicBrainz::Track:0x0000563bd6dc0920[0m
          @length[32m=[0m[1;34m197000[0m,
          @position[32m=[0m[1;34m4[0m,
          @recording_id[32m=[0m[31m[1;31m"[0m[31mdb828412-ac84-4caa-ae0c-da49b3cc2929[1;31m"[0m[31m[0m,
          @title[32m=[0m[31m[1;31m"[0m[31mRight Off[1;31m"[0m[31m[0m[32m>[0m,
         [32m#<MusicBrainz::Track:0x0000563bd9400900[0m
          @length[32m=[0m[1;34m352000[0m,
          @position[32m=[0m[1;34m5[0m,
          @recording_id[32m=[0m[31m[1;31m"[0m[31m08781184-dc67-4135-8771-165ea04f4a8d[1;31m"[0m[31m[0m,
          @title[32m=[0m[31m[1;31m"[0m[31mFunk[1;31m"[0m[31m[0m[32m>[0m,
         [32m#<MusicBrainz::Track:0x0000563bd9446d60[0m
          @length[32m=[0m[1;34m1405000[0m,
          @position[32m=[0m[1;34m6[0m,
          @recording_id[32m=[0m[31m[1;31m"[0m[31m44af35a6-b2df-4f30-a2b7-95ed06aa72bb[1;31m"[0m[31m[0m,
          @title[32m=[0m[31m[1;31m"[0m[31mCalypso Frelimo[1;31m"[0m[31m[0m[32m>[0m][32m>[0m],
    @title[32m=[0m[31m[1;31m"[0m[31mLive in Vienna 1973[1;31m"[0m[31m[0m,
    @type[32m=[0m[31m[1;31m"[0m[31mLive[1;31m"[0m[31m[0m[32m>[0m],
 @type[32m=[0m[31m[1;31m"[0m[31mGroup[1;31m"[0m[31m[0m,
 @urls[32m=[0m{[33m:discogs[0m=>[31m[1;31m"[0m[31mhttps://www.discogs.com/artist/1900148[1;31m"[0m[31m[0m}[32m>[0m
